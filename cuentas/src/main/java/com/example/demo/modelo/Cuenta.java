package com.example.demo.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "Cuenta")
public class Cuenta {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) private Long id;
	@ManyToOne @JoinColumn(name = "id_pais") private Pais pais;
	private String nombre;

	public Cuenta() {
	}
	
	public Cuenta(String nombre, Pais pais) {
		super();
		this.nombre = nombre;
		this.pais = pais;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

}
