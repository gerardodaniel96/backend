package com.example.demo.service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;
import org.springframework.stereotype.Service;

import com.example.demo.modelo.Cuenta;
import com.example.demo.repository.CuentaRepository;

@Service
public class CuentaService implements CuentaRepository {

	@Autowired private CuentaRepository cuentaRepository;

	@Override
	public List<Cuenta> findAll() {
		return cuentaRepository.findAll();
	}

	@Override
	public List<Cuenta> findAll(Sort sort) {
		return cuentaRepository.findAll(sort);
	}

	@Override
	public List<Cuenta> findAllById(Iterable<Long> ids) {
		return cuentaRepository.findAllById(ids);
	}

	@Override
	public <S extends Cuenta> List<S> saveAll(Iterable<S> entities) {
		return cuentaRepository.saveAll(entities);
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends Cuenta> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Cuenta> List<S> saveAllAndFlush(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllInBatch(Iterable<Cuenta> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllByIdInBatch(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		cuentaRepository.deleteAllInBatch();
	}

	@Override
	public Cuenta getOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cuenta getById(Long id) {
		return cuentaRepository.getById(id);
	}

	@Override
	public <S extends Cuenta> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Cuenta> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Cuenta> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Cuenta> S save(S entity) {
		return cuentaRepository.save(entity);
	}

	@Override
	public Optional<Cuenta> findById(Long id) {
		// TODO Auto-generated method stub
		return cuentaRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return cuentaRepository.existsById(id);
	}

	@Override
	public long count() {
		return cuentaRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		cuentaRepository.deleteById(id);
	}

	@Override
	public void delete(Cuenta entity) {
		cuentaRepository.delete(entity);
	}

	@Override
	public void deleteAllById(Iterable<? extends Long> ids) {
		cuentaRepository.deleteAllById(ids);
	}

	@Override
	public void deleteAll(Iterable<? extends Cuenta> entities) {
		cuentaRepository.deleteAll(entities);
	}

	@Override
	public void deleteAll() {
		cuentaRepository.deleteAll();
	}

	@Override
	public <S extends Cuenta> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Cuenta> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Cuenta> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends Cuenta> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <S extends Cuenta, R> R findBy(Example<S> example, Function<FetchableFluentQuery<S>, R> queryFunction) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
