package com.example.demo.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.modelo.Cuenta;
import com.example.demo.service.CuentaService;

@RestController
@RequestMapping("/cuentas/")
public class CuentaREST {
	
	@Autowired private CuentaService cuentaService;

	@GetMapping private ResponseEntity<List<Cuenta>> getAllCuentas() {
		return ResponseEntity.ok(cuentaService.findAll());
	}
	
	@GetMapping(value = "{id}") private ResponseEntity<Cuenta> getCuenta(@PathVariable("id") Long idCuenta) {
		if (cuentaService.findById(idCuenta) != null) {
			return ResponseEntity.ok(cuentaService.getById(idCuenta));
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@PostMapping private ResponseEntity<Cuenta> saveCuenta(@RequestBody Cuenta cuenta) {
		try {
			Cuenta cuentaGuardada = cuentaService.save(cuenta);
			return ResponseEntity.created(new URI("/cuentas/"+cuenta.getId())).body(cuentaGuardada);
		}
		catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PutMapping(value = "{id}") private ResponseEntity<Cuenta> updateCuenta(@PathVariable("id") Long idCuenta, @RequestBody Cuenta cuenta) {
		if (cuentaService.findById(idCuenta) != null) {
			try {
				Cuenta cuentaGuardada = cuentaService.save(cuenta);
				return ResponseEntity.accepted().body(cuentaGuardada);
			}
			catch (Exception e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@PatchMapping(value = "{id}") private ResponseEntity<Cuenta> patchCuenta(@PathVariable("id") Long idCuenta, @RequestBody Cuenta cuenta) {
		if (cuentaService.findById(idCuenta) != null) {
			try {
				Cuenta cuentaGuardada = cuentaService.save(cuenta);
				return ResponseEntity.accepted().body(cuentaGuardada);
			}
			catch (Exception e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@DeleteMapping(value = "{id}") private ResponseEntity<Boolean> deleteCuenta(@PathVariable("id") Long idCuenta) {
		if (cuentaService.findById(idCuenta) != null) {
			cuentaService.deleteById(idCuenta);
			return ResponseEntity.ok( cuentaService.findById(idCuenta) != null );
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
}
